FROM openjdk:17-alpine3.12
ADD target/maru-actuator-1.jar maru-actuator-1.jar
ENTRYPOINT ["java", "-jar", "maru-actuator-1.jar"]