package com.example.maruactuator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaruActuatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaruActuatorApplication.class, args);
	}

}
